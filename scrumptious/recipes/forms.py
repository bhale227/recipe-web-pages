from django import forms
from django.forms import ModelForm
from recipes.models import Recipe


class RecipeForm(ModelForm):
    email_address = forms.EmailField(max_length=300)

    class Meta:
        model = Recipe
        fields = [
            "title",
            "picture",
            "description",
        ]
#class SignUpForm(forms.Form):
#    username = forms.CharField(max_length=150)
#    first_name = forms.CharField(max_length=150)
#    last_name = forms.CharField(max_length=150)
#    password = forms.CharField(
#        max_length=150,
#        widget=forms.PasswordInput,
#    )
