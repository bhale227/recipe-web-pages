# Generated by Django 5.0 on 2023-12-11 22:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0004_recipe_author'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='recipestep',
            options={'ordering': ['step_number']},
        ),
        migrations.RenameField(
            model_name='recipestep',
            old_name='order',
            new_name='step_number',
        ),
    ]
